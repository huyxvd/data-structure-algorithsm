#include <stdio.h>

// Hàm tìm kiếm nhị phân cho mảng tăng dần
int binarySearch(int* arr, int size, int target) {
    int low = 0;
    int high = size - 1;

    while (low <= high) {
        int mid = (high + low) / 2; // Tính chỉ số trung bình

        if (arr[mid] == target) {
            return mid; // Trả về vị trí nếu tìm thấy phần tử
        }
        else if (arr[mid] < target) {
            low = mid + 1; // Tìm kiếm trong nửa phải của mảng
        }
        else {
            high = mid - 1; // Tìm kiếm trong nửa trái của mảng
        }
    }

    return -1; // Trả về -1 nếu không tìm thấy phần tử
}

int main() {
    int arr[] = { 2, 5, 8, 12, 16, 23, 38, 45, 67 };
    int size = sizeof(arr) / sizeof(arr[0]);

    int target = 17;
    int index = binarySearch(arr, size, target);

    if (index != -1) {
        printf("Phan tu %d duoc tim thay tai vi tri %d\n", target, index);
    }
    else {
        printf("Phan tu %d khong co trong mang\n", target);
    }

    return 0;
}

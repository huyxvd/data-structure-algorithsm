#include <stdio.h>

// Hàm tìm kiếm tuyến tính
int linearSearch(int* arr, int size, int target) {
    int* ptr = arr; // Con trỏ trỏ tới phần tử đầu mảng

    for (int i = 0; i < size; i++) {
        if (*ptr == target) {
            return i; // Trả về vị trí của phần tử nếu tìm thấy
        }
        ptr++; // Di chuyển con trỏ đến phần tử tiếp theo
    }

    return -1; // Trả về -1 nếu không tìm thấy phần tử
}

int main() {
    int arr[] = {2, 5, 8, 12, 16, 23, 38, 45, 67};
    int size = sizeof(arr) / sizeof(arr[0]);
    int target = 23;

    int result = linearSearch(arr, size, target);

    if (result != -1) {
        printf("Phan tu %d duoc tim thay tai vi tri %d\n", target, result);
    } else {
        printf("Phan tu %d khong duoc tim thay trong mang\n", target);
    }

    return 0;
}

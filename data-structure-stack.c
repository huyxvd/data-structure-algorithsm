#include <stdio.h>
#include <stdbool.h>

#define MAX_SIZE 100
// triển khai stack sử dụng mảng để lưu trữ dữ liệu
typedef struct {
    int arr[MAX_SIZE];
    int top;
} Stack;

void initStack(Stack* stack) {
    stack->top = -1;
}

void push(Stack* stack, int item) {
    if (stack->top == MAX_SIZE - 1) {
        printf("Stack is full.\n");
        return;
    }

    stack->top++;
    stack->arr[stack->top] = item;
}

int pop(Stack* stack) {
    if (stack->top == -1) {
        printf("Stack is empty.\n");
        return -1;
    }

    int item = stack->arr[stack->top];
    stack->top--;
    return item;
}

int top(Stack* stack) {
    if (stack->top == -1) {
        printf("Stack is empty.\n");
        return -1;
    }

    return stack->arr[stack->top];
}

bool isEmpty(Stack* stack) {
    return (stack->top == -1);
}

bool isFull(Stack* stack) {
    return (stack->top == MAX_SIZE - 1);
}

int main() {
    Stack myStack;
    initStack(&myStack);

    push(&myStack, 5);
    push(&myStack, 3);
    push(&myStack, 8);
    push(&myStack, 2);

    printf("Top item: %d\n", top(&myStack));

    int poppedItem = pop(&myStack);
    printf("Popped item: %d\n", poppedItem);

    return 0;
}

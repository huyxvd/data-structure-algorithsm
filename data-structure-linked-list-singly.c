#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>

struct Node {
	int data; // dữ liệu
	struct Node *next; // con trỏ đến Node kế tiếp
};

// Duyệt qua danh sách liên kết
void printLinkedlist(struct Node* p) {
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
}


struct Node* findNode(struct Node* head, int value) {
	struct Node* current = head;

	while (current != NULL) {
		if (current->data == value) {
			return current;
		}

		current = current->next;
	}

	return NULL;
}

void addNodeAtBeginning(struct Node **headRef, int value) {
	struct Node* newNode = malloc(sizeof(struct Node));
	newNode->data = value;
	newNode->next = *headRef;
	*headRef = newNode;
}

void addNodeAtEnd(struct Node* head, int value) {
	struct Node* newNode = malloc(sizeof(struct Node));
	newNode->data = value;
	newNode->next = NULL;

	if (head == NULL) {
		head = newNode;
	}
	else {
		struct Node* current = head;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newNode;
	}
}

void addNodeAfter(struct Node* prevNode, int value) {
	if (prevNode == NULL) {
		return;
	}

	struct Node* newNode = malloc(sizeof(struct Node));
	newNode->data = value;
	newNode->next = prevNode->next;
	prevNode->next = newNode;
}

// Hàm để xóa node ở vị trí đầu tiên trong danh sách liên kết đơn
void deleteFirstNode(struct Node** head) {
	// Kiểm tra xem danh sách có rỗng hay không
	if (*head == NULL) {
		return;
	}

	// Lưu trữ con trỏ đến node đầu tiên
	struct Node* temp = *head;

	// Cập nhật con trỏ head để trỏ đến node thứ hai
	*head = (*head)->next;

	// Giải phóng bộ nhớ của node đầu tiên
	free(temp);
}

void deleteLastNode(struct Node** head) {
	if (*head == NULL) {
		return;
	}

	// Nếu danh sách chỉ có một node
	if ((*head)->next == NULL) {
		free(*head);
		*head = NULL;
		return;
	}

	// Duyệt từ đầu đến cuối danh sách
	struct Node* current = *head;
	struct Node* prev = NULL;
	while (current->next != NULL) {
		prev = current;
		current = current->next;
	}

	// Giải phóng bộ nhớ của node cuối cùng
	free(current);
	prev->next = NULL;
}

int main() {
	/* khởi tạotạo node */
	struct Node* head = NULL;
	struct Node* one = NULL;
	struct Node* two = NULL;
	struct Node* three = NULL;

	/* Khởi tạo vùng nhớ động */
	one = malloc(sizeof(struct Node));
	two = malloc(sizeof(struct Node));
	three = malloc(sizeof(struct Node));

	/* gán giá trị cho từng Node */
	one->data = 1;
	two->data = 2;
	three->data = 3;

	/* Nối các Node lại với nhau */
	one->next = two;
	two->next = three;
	three->next = NULL;

	/* trỏ node đầu vào Node đầu tiên trong danh sách */
	head = one;

	printf("printLinkedlist: \n");
	printLinkedlist(head);
	printf("\nfindNode(3): \n");

	struct Node *node3 = findNode(head, 3);
	if (node3 != NULL) {
		printf("tim thay %d");
	}
	else {
		printf("khong tim thay");
	}

	printf("\naddNodeAtBeginning Node(100): \n");

	addNodeAtBeginning(&head, 100);
	printLinkedlist(head);

	printf("\naddNodeAtEnd Node(99): \n");
	addNodeAtEnd(head, 99);
	printLinkedlist(head);

	printf("\naddNodeAfter Node(99) with value 33: \n");
	struct Node* node99 = findNode(head, 99);
	addNodeAfter(node99, 33);
	printLinkedlist(head);

	printf("\ndeleteFirstNode \n");
	deleteFirstNode(&head);
	printLinkedlist(head);

	printf("\ndeleteLastNode \n");
	deleteLastNode(&head);
	printLinkedlist(head);

	return 0;
}


#include<stdio.h>
#include<stdlib.h>

struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};

void printLinkedList(struct Node* head) {
    struct Node* current = head;

    while (current != NULL) {
        printf("%d ", current->data);
        current = current->next;
    }

    printf("\n");
}

void insertAtBeginning(struct Node** headRef, int value) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->data = value;
    newNode->prev = NULL;
    newNode->next = *headRef;

    if (*headRef != NULL) {
        (*headRef)->prev = newNode;
    }

    *headRef = newNode;
}

void insertAtEnd(struct Node** headRef, int value) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->data = value;
    newNode->next = NULL;

    if (*headRef == NULL) {
        newNode->prev = NULL;
        *headRef = newNode;
        return;
    }

    struct Node* current = *headRef;
    while (current->next != NULL) {
        current = current->next;
    }

    current->next = newNode;
    newNode->prev = current;
}

struct Node* findNode(struct Node* head, int value) {
    struct Node* current = head;

    while (current != NULL) {
        if (current->data == value) {
            return current; // Trả về node chứa giá trị tìm
        }
        current = current->next;
    }

    return NULL; // Trả về NULL nếu không tìm thấy node
}

void insertBeforeNode(struct Node** headRef, struct Node* node, int value) {
    if (node == NULL) {
        printf("Cannot insert before a NULL node.\n");
        return;
    }

    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->data = value;
    newNode->prev = node->prev;
    newNode->next = node;

    if (node->prev != NULL) {
        node->prev->next = newNode;
    }
    else {
        *headRef = newNode;
    }

    node->prev = newNode;
}

void insertAfterNode(struct Node* node, int value) {
    if (node == NULL) {
        printf("Cannot insert after a NULL node.\n");
        return;
    }

    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->data = value;
    newNode->next = node->next;
    newNode->prev = node;

    if (node->next != NULL) {
        node->next->prev = newNode;
    }

    node->next = newNode;
}

void removeNode(struct Node** headRef, struct Node* node) {
    if (node == NULL) {
        printf("Cannot remove a NULL node.\n");
        return;
    }

    if (node->prev != NULL) {
        node->prev->next = node->next;
    }
    else {
        *headRef = node->next;
    }

    if (node->next != NULL) {
        node->next->prev = node->prev;
    }

    free(node);
}

int main() {
    struct Node* head = NULL;

    insertAtBeginning(&head, 3);
    insertAtBeginning(&head, 2);
    insertAtBeginning(&head, 1);

    insertAtEnd(&head, 4);
    insertAtEnd(&head, 5);
    insertAtEnd(&head, 6);

    printLinkedList(head);

    struct Node* nodeToInsertBefore = findNode(head, 4);
    insertBeforeNode(&head, nodeToInsertBefore, 10);
    printLinkedList(head);

    struct Node* nodeToInsertAfter = findNode(head, 5);
    insertAfterNode(nodeToInsertAfter, 20);
    printLinkedList(head);

    struct Node* nodeToRemove = findNode(head, 2);
    removeNode(&head, nodeToRemove);
    printLinkedList(head);

    return 0;
}
